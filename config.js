// Copyright 2023 Daniel Erat.
// All rights reserved.

// localStorage key for JSON-marshaled config object.
const CONFIG_KEY = 'subtitle-viewer';

// Maximum number of recent files to save positions for.
const MAX_POSITIONS = 10;

// Saves the supplied position (in seconds) for the named file.
export function savePosition(file, pos) {
  const cfg = loadConfig();

  let entry = null;
  const idx = cfg.positions.findIndex((e) => e.file === file);
  if (idx !== -1) {
    entry = cfg.positions[idx];
    entry.pos = pos;
    cfg.positions.splice(idx, 1);
  } else {
    entry = { file, pos };
  }

  // Put the entry at the front of the array and truncate it if needed.
  cfg.positions.unshift(entry);
  if (cfg.positions.length > MAX_POSITIONS) cfg.positions.splice(MAX_POSITIONS);

  saveConfig(cfg);
}

// Returns the saved position for the named file, or null if no position is
// saved.
export function loadPosition(file) {
  return loadConfig().positions.find((e) => e.file === file)?.pos ?? null;
}

// Loads the config from localStorage.
function loadConfig() {
  const json = localStorage.getItem(CONFIG_KEY) ?? '{}';
  let cfg = {};
  try {
    cfg = JSON.parse(json);
  } catch (e) {
    console.error(`Ignoring bad config ${JSON.stringify(json)}: ${e}`);
  }

  // Set default values.
  cfg.positions ||= []; // { file: 'foo.srt', pos: 3245.34 }

  return cfg;
}

// Saves |cfg| to localStorage.
function saveConfig(cfg) {
  localStorage.setItem(CONFIG_KEY, JSON.stringify(cfg));
}
