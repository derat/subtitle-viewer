// Copyright 2023 Daniel Erat.
// All rights reserved.

import { getNow } from './common.js';

// Duration to show subtitles with invalid (e.g. negative) durations.
const badDurationSec = 1.0;

// The Timeline class holds a sequence of subtitles.
export class Timeline {
  // Subtitle objects:
  // {
  //   counter: 2,
  //   start: 54.3,
  //   end: 56.2,
  //   text: ['First line', 'and second.'],
  // }
  #subs = [];

  // Ordered objects listing which subtitles should be visible at a given time:
  // [
  //   {t: 0,   subs: []},       // nothing shown
  //   {t: 0.5, subs: [s0]},     // s0 is shown
  //   {t: 2.5, subs: []},       // s0 is hidden
  //   {t: 5.1, subs: [s1]},     // s1 is shown
  //   {t: 6.5, subs: [s1, s2]}, // s2 is also shown
  //   {t: 7.0, subs: [s2]},     // s1 is hidden
  //   {t: 8.4, subs: []},       // s2 is hidden
  // ]
  #timeline = [{ t: 0, subs: [] }];

  #startTime = 0; // wall seconds at which playback started
  #startPos = 0; // subtitle seconds at which playback started
  #pausePos = 0; // subtitle seconds at which playback was paused
  #curIndex = -1; // current index into #timeline
  #timeoutId = -1; // ID of timeout for calling #onTimeout()
  #updateCallback = null; // invoked with array of objects from #subs
  #stoppedCallback = null; // invoked when playback stops at the end of the file

  // |srt| is a string containing SRT subtitle data to parse.
  // See https://en.wikipedia.org/wiki/SubRip for details.
  constructor(srt, updateCallback, stoppedCallback) {
    this.#subs = parseSRT(srt);
    this.#updateCallback = updateCallback;
    this.#stoppedCallback = stoppedCallback;

    const events = [];
    for (const sub of this.#subs) {
      events.push({ t: sub.start, show: sub });
      events.push({ t: sub.end, hide: sub });
    }
    events.sort((a, b) => a.t - b.t);

    let shown = []; // currently-shown subtitle objects
    for (const ev of events) {
      if (ev.show) shown.push(ev.show);
      if (ev.hide) shown = shown.filter((s) => s !== ev.hide);

      if (this.#timeline.at(-1).t === ev.t) {
        this.#timeline.at(-1).subs = [...shown];
      } else {
        this.#timeline.push({ t: ev.t, subs: [...shown] });
      }
    }
  }

  get numSubs() {
    return this.#subs.length;
  }
  get duration() {
    return this.#timeline.at(-1).t;
  }
  get position() {
    return this.paused
      ? this.#pausePos
      : this.#startPos + (getNow() - this.#startTime);
  }
  get paused() {
    return this.#startTime <= 0;
  }
  get nextPosition() {
    const idx = this.#curIndex >= 0 ? this.#curIndex + 1 : 1;
    return this.#timeline[idx]?.t ?? -1;
  }

  // Starts playback at the current position.
  play() {
    if (!this.paused || this.#pausePos >= this.duration) return;

    console.log(`Playing at ${this.position.toFixed(3)}`);
    this.#startTime = getNow();
    this.#startPos = this.#pausePos;
    this.#pausePos = 0;

    this.#curIndex = this.#findIndex(this.#startPos);
    this.#notifyUpdate();
    this.#scheduleTimeout();
  }

  // Pauses playback.
  pause() {
    if (this.paused) return;

    console.log(`Pausing at ${this.position.toFixed(3)}`);
    this.#pausePos = this.position;
    this.#startTime = 0;
    this.#startPos = 0; // sets |paused| to true
    this.#cancelTimeout();
  }

  // Seeks to the specified position in seconds.
  // The playback state is unchanged.
  seek(pos) {
    if (pos < 0 || pos >= this.duration) {
      console.error(`Ignoring request to seek to invalid position ${pos}`);
      return;
    }

    console.log(`Seeking to ${pos.toFixed(3)}`);
    this.#curIndex = this.#findIndex(pos);
    if (this.paused) {
      this.#pausePos = pos;
    } else {
      this.#startPos = pos;
      this.#startTime = getNow();
      this.#scheduleTimeout();
    }
    this.#notifyUpdate();
  }

  // Schedules a call to #onTimeout() and sets #timeoutId.
  #scheduleTimeout() {
    if (this.paused) return;
    this.#cancelTimeout();
    const next = this.#timeline[this.#curIndex + 1];
    if (!next) {
      this.pause();
      this.#stoppedCallback();
      return;
    }
    const delayMs = 1000 * (next.t - this.position);
    this.#timeoutId = window.setTimeout(() => this.#onTimeout(), delayMs);
  }

  // Cancels and clears #timeoutId if set.
  #cancelTimeout() {
    if (this.#timeoutId <= 0) return;
    window.clearTimeout(this.#timeoutId);
    this.#timeoutId = -1;
  }

  // Called by #timeoutId to update subtitles.
  #onTimeout() {
    this.#timeoutId = -1;
    // TODO: Doing a binary search again isn't really necessary here;
    // we can probably just seek forward from #curIndex.
    this.#curIndex = this.#findIndex(this.position);
    this.#notifyUpdate();
    this.#scheduleTimeout();
  }

  // Passes subtitles corresponding to #curIndex to #updateCallback.
  #notifyUpdate() {
    const entry = this.#timeline[this.#curIndex];
    if (!entry) {
      console.error(`Can't notify with bad index ${this.#curIndex}`);
      return;
    }
    this.#updateCallback(entry.subs);
  }

  // Returns the index into #timeline of the object corresponding to time |t|
  // (given in seconds).
  #findIndex(t) {
    // Blegh, binary search.
    let start = 0;
    let end = this.#timeline.length - 1;

    while (start <= end) {
      let mid = Math.floor((start + end) / 2);
      let ent = this.#timeline.at(mid);

      if (
        ent.t <= t &&
        (mid + 1 >= this.#timeline.length || this.#timeline.at(mid + 1).t > t)
      ) {
        return mid;
      } else if (t < ent.t) {
        end = mid - 1;
      } else {
        start = mid + 1;
      }
    }

    return -1;
  }
}

// Parses the supplied SRT data into an array of subtitle objects.
function parseSRT(text) {
  const pgs = text
    .replaceAll('\r\n', '\n')
    .split('\n\n')
    .map((s) => s.trim())
    .filter(Boolean);

  const subs = [];
  for (const pg of pgs) {
    const lines = pg.split('\n');

    // Skip subtitles that are missing the counter: some files seem to have an
    // initial subtitle listing the site from which they were downloaded.
    if (lines.length < 3) {
      console.log(`Skipping missing counter:\n${pg}`);
      continue;
    }
    if (!lines[0].match(/^\d+\s*$/)) {
      console.log(`Skipping bad counter:\n${pg}`);
      continue;
    }

    // TODO: Wikipedia says that text coordinates can be specified at the end of
    // this line as "X1:… X2:… Y1:… Y2:…". I should probably either ignore or
    // honor those.
    const m = lines[1].match(
      /^(\d{2}):(\d{2}):(\d{2}),(\d{3}) --> (\d{2}):(\d{2}):(\d{2}),(\d{3})\s*$/
    );
    if (!m) {
      console.log(`Skipping bad times:\n${pg}`);
      continue;
    }
    const start = makeTime(m[1], m[2], m[3], m[4]);
    let end = makeTime(m[5], m[6], m[7], m[8]);

    if (end < start) {
      console.log(`Subtitle has negative duration:\n${pg}`);
      end = start + badDurationSec;
    }

    subs.push({
      counter: parseInt(lines[0]),
      start,
      end,
      text: lines.slice(2),
    });
  }
  return subs;
}

// makeTime returns a decimal number of seconds from the supplied strings
// containing hours, minutes, seconds, and milliseconds.
const makeTime = (h, m, s, ms) =>
  3600 * parseInt(h) + 60 * parseInt(m) + parseInt(s) + parseInt(ms) / 1000;
