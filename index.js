// Copyright 2023 Daniel Erat.
// All rights reserved.

import { $, createElement, formatSeconds, getNow } from './common.js';
import { loadPosition, savePosition } from './config.js';
import { Timeline } from './timeline.js';

// Amounts to seek for Page Up/Down, Up/Down, and Left/Right.
const SEEK_HUGE_SEC = 60;
const SEEK_BIG_SEC = 10;
const SEEK_SMALL_SEC = 1;

// Amount to adjust font scale for plus or minus keys.
const ADJUST_FONT_AMOUNT = 0.1;

// Duration to show status after play/pause or seek, in seconds.
const STATUS_SHOW_SEC = 4;

const fileInput = $('file-input');
const fileButton = $('file-button');
const subtitleContainer = $('subtitle-container');
const statusDiv = $('status-div');
const messageDiv = $('message-div');
const pauseIcon = $('pause-icon');
const playIcon = $('play-icon');
const stopIcon = $('stop-icon');
const timeSpan = $('time-span');
const helpDiv = $('help-div');

let timeline = null; // Timeline object holding subtitles
let filename = null; // name of loaded subtitle file
let statusIntervalId = -1; // calls updateStatus() while shown
let statusTimeoutId = -1; // hides status
let messageText = ''; // text to display in |messageDiv|
let messageTime = -1; // time at which |messageText| was updated
let wakeLock = null;

// Reads a subtitle file from |fileInput| and hides |fileButton|.
function readFile() {
  const files = fileInput.files;
  if (!files.length) return;
  const fr = new FileReader();
  fr.addEventListener('load', () => {
    timeline = new Timeline(fr.result, updateSubs, showStatus);
    filename = files[0].name;

    const pos = loadPosition(filename);
    if (pos !== null) timeline.seek(pos);

    const msg =
      `${timeline.numSubs} subtitles - ` +
      `${formatSeconds(timeline.duration, true)}`;
    console.log(msg);
    showMessage(msg + "\n'?' for help");
    window.document.title = `${filename} - Subtitle Viewer`;

    fileButton.classList.add('hidden');
    fileInput.value = null;
  });
  fr.readAsText(files[0]);
}

// Displays the supplied subtitles.
function updateSubs(subs) {
  const cont = subtitleContainer;
  while (cont.firstChild) cont.removeChild(cont.lastChild);
  for (const sub of subs) {
    const fmt = {};
    for (const line of sub.text) {
      const div = createElement('div', cont);
      renderLine(line, div, fmt);
    }
  }
}

// Parses the supplied line of subtitle text and creates spans under |div|.
// |fmt| is an object used to track formatting state; the same object should be
// passed for all lines in a given subtitle.
function renderLine(line, div, fmt) {
  // TODO: SRT files that were improperly converted sometimes contain "{\an8}"
  // at the beginning of lines. This is apparently used by the ASS/SSA format to
  // center subtitles at the top of the screen:
  // https://www.reddit.com/r/PleX/comments/11zpjky/unknown_text_in_subtitles_all_of_a_sudden_an8/
  // https://emby.media/community/index.php?/topic/119556-an8-subrip-problem/
  //
  // I could probably position the subtitle div accordingly here, but I'm not
  // sure it's worth the effort since I'm usually just displaying subtitles
  // fullscreen on a second screen.
  //
  // Full (?) list from the emby thread above:
  //   an7: top left      an8: top center      an9: top right
  //   an4: middle left   an5: middle center   an6: middle right
  //   an1: bottom left   an2: bottom center   an3: bottom right
  line = line.replace(/^\{\\an[1-9]\}/, '');

  for (const part of line.split(/(<[^>]+>)/)) {
    const fontMatch = part.match(/^<font\s+color="(.+)">$/);
    if (part === '') {
      continue;
    } else if (part === '<b>') {
      fmt.bold = true;
    } else if (part === '</b>') {
      fmt.bold = false;
    } else if (part === '<i>') {
      fmt.italic = true;
    } else if (part === '</i>') {
      fmt.italic = false;
    } else if (part === '<u>') {
      fmt.underline = true;
    } else if (part === '</u>') {
      fmt.underline = false;
    } else if (fontMatch) {
      fmt.color = fontMatch[1];
    } else if (part === '</font>') {
      fmt.color = null;
    } else {
      const span = createElement('span', div, null, part);
      // Avoid formatting CJK test.
      // TODO: Figure out if more subtlety is needed here, or if it'd be better
      // to just render subtitles as instructed.
      if (!part.match(cjkRegExp)) {
        if (fmt.bold) span.style.fontWeight = 'bold';
        if (fmt.italic) span.style.fontStyle = 'italic';
        if (fmt.underline) span.style.textDecoration = 'underline';
      }
      if (fmt.color) span.style.color = fmt.color;
    }
  }
}

// Matches various ranges used for Chinese, Japanese, and Korean text:
// https://stackoverflow.com/a/43419070
const cjkRegExp =
  /[\u3040-\u30ff\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff\uff66-\uff9f]/;

// Seeks relative to the current position and calls showStatus().
function seekRel(sec) {
  const pos = Math.max(timeline.position + sec, 0);
  if (pos < timeline.duration) {
    timeline.seek(pos);
    showStatus();
  }
}

// Increases or decreases the subtitle font's scale by |amount| (e.g. 0.1).
function adjustFont(amount) {
  const scale = (
    parseFloat(
      getComputedStyle(subtitleContainer).getPropertyValue('--font-scale')
    ) + amount
  ).toFixed(1);
  if (isNaN(scale) || scale <= 0) return;
  showMessage(`Scaling font by ${scale}`);
  subtitleContainer.style.setProperty('--font-scale', scale);
}

// Handles 'keydown' events.
function handleKeyDown(ev) {
  if (!timeline) return;

  if (
    (() => {
      if (ev.key === 'Enter') {
        setSubtitlesFillWindow(!subtitlesFillWindow());
        return true;
      } else if (ev.key === '+') {
        adjustFont(ADJUST_FONT_AMOUNT);
        return true;
      } else if (ev.key === '-') {
        adjustFont(-ADJUST_FONT_AMOUNT);
        return true;
      }

      // Everything else requires subtitles to already be loaded.
      if (!timeline) return false;

      if (ev.key === ' ') {
        if (timeline.paused) {
          timeline.play();
        } else {
          timeline.pause();
          maybeSavePosition();
        }
        showStatus();
        return true;
      } else if (ev.key === 'PageUp') {
        seekRel(-SEEK_HUGE_SEC);
        return true;
      } else if (ev.key === 'PageDown') {
        seekRel(SEEK_HUGE_SEC);
        return true;
      } else if (ev.key === 'ArrowUp') {
        seekRel(-SEEK_BIG_SEC);
        return true;
      } else if (ev.key === 'ArrowDown') {
        seekRel(SEEK_BIG_SEC);
        return true;
      } else if (ev.key === 'ArrowLeft') {
        seekRel(-SEEK_SMALL_SEC);
        return true;
      } else if (ev.key === 'ArrowRight') {
        seekRel(SEEK_SMALL_SEC);
        return true;
      } else if (ev.key === 'f') {
        toggleFullscreen();
        return true;
      } else if (ev.key === 'n') {
        const pos = timeline.nextPosition;
        if (pos >= 0) showMessage(`Next at ${formatSeconds(pos)}`);
        return true;
      } else if (ev.key === '?') {
        helpDiv.classList.toggle('shown');
        return true;
      } else if (ev.key === 'Escape') {
        if (helpDiv.classList.contains('shown')) {
          helpDiv.classList.remove('shown');
          return true;
        }
        // The Escape key automatically exits fullscreen, so we don't need to
        // handle that here.
      }

      return false;
    })()
  ) {
    ev.preventDefault();
    ev.stopPropagation();
  }
}

// Displays the status for a few seconds.
function showStatus() {
  if (statusIntervalId > 0) window.clearInterval(statusIntervalId);
  if (statusTimeoutId > 0) window.clearTimeout(statusTimeoutId);

  updateStatus();
  statusDiv.classList.add('shown');

  statusIntervalId = window.setInterval(updateStatus, 1000);
  statusTimeoutId = window.setTimeout(() => {
    window.statusTimeoutId = -1;
    window.clearInterval(statusIntervalId);
    window.statusIntervalId = -1;
    statusDiv.classList.remove('shown');
  }, STATUS_SHOW_SEC * 1000);
}

// Updates the status that's displayed by showStatus().
function updateStatus() {
  messageDiv.innerText =
    messageTime > 0 && getNow() - messageTime <= STATUS_SHOW_SEC
      ? messageText
      : '';
  timeSpan.innerText = formatSeconds(timeline.position);
  const show =
    timeline.position >= timeline.duration
      ? stopIcon
      : timeline.paused
        ? pauseIcon
        : playIcon;
  for (const icon of [pauseIcon, playIcon, stopIcon]) {
    if (icon === show) icon.classList.add('shown');
    else icon.classList.remove('shown');
  }
}

// Shows |text| in the status area.
function showMessage(text) {
  messageText = text;
  messageTime = getNow();
  showStatus();
}

// Toggles the app's fullscreen state.
function toggleFullscreen() {
  // Only change the subtitles fill vs. at bottom setting when we enter
  // fullscreen, since that's the common case at startup.
  if (document.fullscreenElement) {
    document.exitFullscreen();
  } else {
    document.documentElement.requestFullscreen();
    setSubtitlesFillWindow(true);
  }
}

// Handles 'fullscreenchange' events.
function handleFullscreenChange() {
  const fullscreen = !!document.fullscreenElement;
  if (fullscreen && !wakeLock) {
    navigator.wakeLock.request('screen').then((lock) => {
      console.log('Acquired wake lock');
      wakeLock = lock;
    });
  } else if (!fullscreen && wakeLock) {
    maybeSavePosition();
    wakeLock.release().then(() => {
      console.log('Released wake lock');
      wakeLock = null;
    });
  }
}

// Returns true if subtitles are set to fill the window.
const subtitlesFillWindow = () => subtitleContainer.classList.contains('fill');

// Sets subtitles to fill the window vs. be at the bottom of the window.
function setSubtitlesFillWindow(fill) {
  if (fill) subtitleContainer.classList.add('fill');
  else subtitleContainer.classList.remove('fill');
  showMessage(`Subtitles ${fill ? 'fill window' : 'at bottom'}`);
}

// Save the current file's position to localStorage.
const maybeSavePosition = () => {
  if (filename && timeline) savePosition(filename, timeline.position);
};

// Initialize the page.
(() => {
  // Make the button trigger the hidden file input.
  fileButton.addEventListener('click', () => fileInput.click());
  fileInput.addEventListener('input', () => readFile());

  document.body.addEventListener('keydown', handleKeyDown);
  document.documentElement.addEventListener(
    'fullscreenchange',
    handleFullscreenChange
  );
  window.addEventListener('beforeunload', () => maybeSavePosition());
  document.addEventListener('visibilitychange', () => {
    if (document.visibilityState === 'hidden') maybeSavePosition();
  });
})();
