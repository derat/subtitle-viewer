# Icon license info

## MFG Labs

The [MFG Labs icon set] from [MFG Labs] is licensed under a
[Creative Commons Attribution 3.0 License].

This repository includes SVG versions of individual icons downloaded from
<https://iconpharm.com/web-app/category/flat/mfglabs-iconset>.

[MFG Labs icon set]: https://mfglabs.github.io/mfglabs-iconset/
[MFG Labs]: https://www.mfglabs.com/
[Creative Commons Attribution 3.0 License]: https://creativecommons.org/licenses/by/3.0/
