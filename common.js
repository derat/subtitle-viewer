// Copyright 2023 Daniel Erat.
// All rights reserved.

export const $ = (id) => document.getElementById(id);

export function createElement(
  type,
  parentElement = null,
  className = null,
  text = null
) {
  const element = document.createElement(type);
  if (parentElement) parentElement.appendChild(element);
  if (className) element.className = className;
  if (text || text === '') element.appendChild(document.createTextNode(text));
  return element;
}

function pad(num, width) {
  let str = num.toString();
  while (str.length < width) str = '0' + str;
  return str;
}

// Returns fractional wall seconds relative to an arbitrary origin.
export const getNow = () => performance.now() / 1000;

// Formats |sec| as 'HH:MM:SS'.
// If |fract| is true, fractional seconds are also included.
export function formatSeconds(sec, fract = false) {
  const hours = Math.floor(sec / 3600);
  sec -= 3600 * hours;
  const mins = Math.floor(sec / 60);
  sec -= 60 * mins;
  // TODO: Round in the non-fractional case?
  const secs = Math.floor(sec);
  sec -= secs;

  let str = `${pad(hours, 2)}:${pad(mins, 2)}:${pad(secs, 2)}`;
  if (fract) str += sec.toFixed(3).slice(1);
  return str;
}
