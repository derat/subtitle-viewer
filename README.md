# subtitle-viewer

This is a web app for viewing [SubRip] `.srt` subtitle files.

To use it, to <https://derat.codeberg.page/subtitle-viewer/@main/> and select a
`.srt` file from your computer.

<img src="https://www.erat.org/programming/subtitle_viewer-1920.png"
     width="640" border="1" alt="[screenshot]">

[SubRip]: https://en.wikipedia.org/wiki/SubRip

## Keyboard controls

*   **Space** - Play/pause
*   **Left** / **Right** - Seek backward/forward by 1 second
*   **Up** / **Down** - Seek backward/forward by 10 seconds
*   **Page Up** / **Page Down** - Seek backward/forward by 1 minute
*   **Enter** - Toggle subtitles filling entire window vs. just at bottom
*   **Plus** / **Minus** - Increase/decrease font size
*   **f** - Toggle fullscreen mode (and keeping screen on)
*   **n** - Display info about the next subtitle event
*   **?** - Display help screen

## Other notes

The [translate-subtitles] program may also be useful.

[translate-subtitles]: https://codeberg.org/derat/translate-subtitles

---

`favicon-16.png` and `favicon-32.png` were created by [Freepik] and downloaded
from <https://www.flaticon.com/free-icon/subtitles_3799074>.

[Freepik]: https://www.freepik.com/
